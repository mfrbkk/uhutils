package com.tmbbank.mf.UnitholderUpdate.service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.tmbbank.mf.UnitholderUpdate.output.domain.UnitholderUpdate;
import com.tmbbank.mf.UnitholderUpdate.output.txt.TxtProducer;
import com.tmbbank.mf.UnitholderUpdate.utils.Log;

public class CBSDataProcessTest
{
	@Test
	public void testGetMFRMID()
	{
		Log.blDebugMode = Boolean.FALSE;

		Set<String> MFRMIDList = CBSDataProcess.getMFRMID();
		Assert.assertEquals(71, MFRMIDList.size());

		List<UnitholderUpdate> uhList = CBSDataProcess.getMFRecordList(MFRMIDList);
		Assert.assertEquals(MFRMIDList.size(), uhList.size());

		TAPDataProcess.getUnitholderUpdateDetails(MFRMIDList, uhList);

		TxtProducer.createFile(TxtProducer.formatData(uhList));

		try
		{
			final Path dir = Paths.get(TxtProducer.getoutputPath());
			Assert.assertTrue(Files.exists(dir));
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
