package com.tmbbank.mf.UnitholderUpdate.service;

import org.junit.Assert;
import org.junit.Test;

public class RMCUSTExtractorTest
{
	@Test
	public void testTitleCodeConverter()
	{
		Assert.assertEquals(RMCUSTExtractor.TITLE_CODE_MR, RMCUSTExtractor.titleCodeConverter("MR"));
		Assert.assertEquals(RMCUSTExtractor.TITLE_CODE_MR, RMCUSTExtractor.titleCodeConverter("MR."));
		Assert.assertEquals(RMCUSTExtractor.TITLE_CODE_MRS, RMCUSTExtractor.titleCodeConverter("MRS "));
		Assert.assertEquals(RMCUSTExtractor.TITLE_CODE_MS, RMCUSTExtractor.titleCodeConverter("Ms."));
		Assert.assertEquals(RMCUSTExtractor.TITLE_CODE_OTHER, RMCUSTExtractor.titleCodeConverter("   "));
		Assert.assertEquals(RMCUSTExtractor.TITLE_CODE_OTHER, RMCUSTExtractor.titleCodeConverter("555"));
		Assert.assertEquals(RMCUSTExtractor.TITLE_CODE_OTHER, RMCUSTExtractor.titleCodeConverter(""));
		Assert.assertEquals(RMCUSTExtractor.TITLE_CODE_OTHER, RMCUSTExtractor.titleCodeConverter("น้องชาย"));
		Assert.assertEquals(RMCUSTExtractor.TITLE_CODE_MR, RMCUSTExtractor.titleCodeConverter("นาย"));
		Assert.assertEquals(RMCUSTExtractor.TITLE_CODE_MS, RMCUSTExtractor.titleCodeConverter("นางสาว   "));
		Assert.assertEquals(RMCUSTExtractor.TITLE_CODE_MS, RMCUSTExtractor.titleCodeConverter("น.ส."));
	}

	@Test
	public void testbuildRMCUSTMapping()
	{
		try
		{
			RMCUSTExtractor.buildRMCUSTMapping();

			Assert.assertNotNull(RMCUSTExtractor.rmcusteFormatMap);
			Assert.assertEquals(221, RMCUSTExtractor.rmcusteFormatMap.size());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
