package com.tmbbank.mf.UnitholderUpdate.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.tmbbank.mf.UnitholderUpdate.output.constant.OutputConstant;
import com.tmbbank.mf.UnitholderUpdate.output.domain.UnitholderUpdate;
import com.tmbbank.mf.UnitholderUpdate.utils.Config;
import com.tmbbank.mf.UnitholderUpdate.utils.Log;

/**
 * 
 * @author Angkoon Pothong
 *
 */
public class CBSDataProcess
{

	public static final String RMCSAC_FILENAME_PROP = "RMCSAC_FILENAME";
	public static final String RMCUST_FILENAME_PROP = "RMCUST_FILENAME";

	// The column position in a line, the position at 33, index 32
	public static final int RMCSAC_MF_COL_INDEX_START = 32;
	public static final int RMCSAC_MF_APP_CODE_LENGTH = 2;
	public static final String RMCSAC_MF_APP_CODE = "97";

	public static final int RMCSAC_RMID_COL_INDEX_START = 2;
	public static final int RMCSAC_RMID_COL_INDEX_END = RMCSAC_MF_COL_INDEX_START;

	/**
	 * Use the old way to access the file in case input file is really large.
	 * Don't wanna read the whole file at once.
	 */
	// TODO change signature
	public static Set<String> getMFRMID()
	{
		Set<String> rmidList = null;
		try
		{
			final BufferedReader buf = getFileBuffer(Config.getProperties(OutputConstant.INPUT_DIR),
					RMCSAC_FILENAME_PROP);

			String line = buf.readLine();
			rmidList = new HashSet<String>();

			while (line != null)
			{
				if (isMFChange(line))
				{
					final String rmid = line.substring(RMCSAC_RMID_COL_INDEX_START, RMCSAC_MF_COL_INDEX_START);
					rmidList.add(rmid);

				}
				line = buf.readLine();
			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		if (Log.blDebugMode)
		{
			for (String rm : rmidList)
			{
				Log.debug("MF RMID [" + rm + "] will be processed");
			}
		}

		return rmidList;
	}

	/**
	 * 
	 * @param line
	 * @return true if application code at col index 34-35 is 97 for mutual fund
	 */
	private static boolean isMFChange(final String line)
	{
		return line.regionMatches(RMCSAC_MF_COL_INDEX_START, RMCSAC_MF_APP_CODE, 0, RMCSAC_MF_APP_CODE_LENGTH);

	}

	/**
	 * A generic file buffer reader where file name can be configured in
	 * properties file
	 * 
	 * @param propName
	 * @return
	 */
	private static BufferedReader getFileBuffer(final String path, final String propName)
	{
		try
		{
			final String filename = Config.getProperties(propName);

			final InputStream is = new FileInputStream(path.trim().concat(filename));
			return new BufferedReader(new InputStreamReader(is, "TIS-620"));

		} catch (Exception e)
		{

			e.printStackTrace();
			return null;
		}

	}

	private static boolean isMFRecord(final String line, final Set<String> rmidList)
	{
		return rmidList.contains(line.substring(3, 33));

	}

	/**
	 * 
	 */
	public static List<UnitholderUpdate> getMFRecordList(final Set<String> rmidList)
	{
		List<UnitholderUpdate> recordList = null;
		Log.debug("rmidList size [" + rmidList.size() + "]");
		try
		{
			final BufferedReader buf = getFileBuffer(Config.getProperties(OutputConstant.INPUT_DIR),
					RMCUST_FILENAME_PROP);

			String rec = buf.readLine();
			recordList = new ArrayList<UnitholderUpdate>();

			int size = 0;
			while (rec != null)
			{
				if (isMFRecord(rec, rmidList))
				{
					size++;
					Log.debug(rec);

					final UnitholderUpdate uh = RMCUSTExtractor.extractRMCUST(rec);
					recordList.add(uh);
				}
				rec = buf.readLine();
			}

			Log.info("Total UH size [" + size + "]");
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return recordList;
	}

}
