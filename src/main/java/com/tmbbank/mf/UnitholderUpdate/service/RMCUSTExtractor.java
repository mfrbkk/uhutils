package com.tmbbank.mf.UnitholderUpdate.service;

import java.text.Collator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tmbbank.mf.UnitholderUpdate.output.constant.OutputConstant;
import com.tmbbank.mf.UnitholderUpdate.output.domain.UnitholderUpdate;
import com.tmbbank.mf.UnitholderUpdate.utils.CSVUtils;
import com.tmbbank.mf.UnitholderUpdate.utils.Config;
import com.tmbbank.mf.UnitholderUpdate.utils.Utils;

/**
 * 
 * @author Angkoon Pothong
 *
 */
public class RMCUSTExtractor
{
	protected static Map<String, String[]> rmcusteFormatMap;
	public static final String RMCUST_FORMAT_PROP_NAME = "RMCUST_FORMAT_FILENAME";

	public static final int RMCUST_VALUE_START_POSITION = 4;
	public static final int RMCUST_VALUE_END_POSITION = 5;
	public static final String RMCUST_MARITAL_STATUS_SINGLE = "U";

	public static final String MARITAL_STATUS_SINGLE = "S";
	public static final String DEFAULT_IAGENT_CODE = "011"; // for TMB
	public static final String GENER_OTHER = "O";
	public static final String GENER_MALE = "M";
	public static final String GENER_FEMALE = "F";
	public static final String UPDATE_STATUS = "U";
	public static final String DEFAULT_FLAG_JOINT = "N";
	public static final String TITLE_CODE_MR = "01";
	public static final String TITLE_CODE_MS = "02";
	public static final String TITLE_CODE_MRS = "03";
	public static final String TITLE_CODE_OTHER = "04";
	public static final String TITLE_MR = "MR";
	public static final String TITLE_MS = "MS";
	public static final String TITLE_MRS = "MRS";
	public static final String Y = "Y";
	public static final String BLANK = "";
	public static final String THAI_COUNTRY_CODE = "THA";

	// RMCUST_FORMAT fields
	public static final String REC_INDICATOR = "REC_INDICATOR";
	public static final String ACTION_CD = "ACTION_CD";
	public static final String CUST_TYP_CD = "CUST_TYP_CD";
	public static final String CUST_ID = "CUST_ID";
	public static final String CIS_CUST_ID = "CIS_CUST_ID";
	public static final String BANK_CODE = "BANK_CODE";
	public static final String BRANCH_CODE = "BRANCH_CODE";
	public static final String BUS_PHONE_EXT = "BUS_PHONE_EXT";
	public static final String BUS_PHONE_NBR = "BUS_PHONE_NBR";
	public static final String CITZN_CD = "CITZN_CD";
	public static final String CITZN_ID = "CITZN_ID";
	public static final String COUNTRY_CD = "COUNTRY_CD";
	public static final String CREDIT_REV_DT = "CREDIT_REV_DT";
	public static final String CREDIT_RATING = "CREDIT_RATING";
	public static final String ISO_CURRENCY_CD = "ISO_CURRENCY_CD";
	public static final String CUST_SINCE_DT = "CUST_SINCE_DT";
	public static final String CUST_TYPE = "CUST_TYPE";
	public static final String DATA_SOURCE = "DATA_SOURCE";
	public static final String DOB = "DOB";
	public static final String DRIV_LIC = "DRIV_LIC";
	public static final String EDUC_LVL_CD = "EDUC_LVL_CD";
	public static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";
	public static final String EMPLMT_DT = "EMPLMT_DT";
	public static final String EMPR_NAME = "EMPR_NAME";
	public static final String FAX_PHONE = "FAX_PHONE";
	public static final String GENDER_CD = "GENDER_CD";
	public static final String INC_BAL = "INC_BAL";
	public static final String INSIDER_CD = "INSIDER_CD";
	public static final String INSIDER_INST_CD = "INSIDER_INST_CD";
	public static final String INSIDER_REL_CD = "INSIDER_REL_CD";
	public static final String LEGAL_STATUS = "LEGAL_STATUS";
	public static final String LOST_CUST_CD = "LOST_CUST_CD";
	public static final String MAIL_CD = "MAIL_CD";
	public static final String MARIT_CD = "MARIT_CD";
	public static final String NAME_1 = "NAME_1";
	public static final String NAME_1ST = "NAME_1ST";
	public static final String NAME_2 = "NAME_2";
	public static final String NAME_3 = "NAME_3";
	public static final String NAME_LST = "NAME_LST";
	public static final String NBR_EMP = "NBR_EMP";
	public static final String OCC_CD = "OCC_CD";
	public static final String OED_CD = "OED_CD";
	public static final String OED_INST_CD = "OED_INST_CD";
	public static final String ORG_UNIT_ID = "ORG_UNIT_ID";
	public static final String OTHER_CARD = "OTHER_CARD";
	public static final String PASSPORT = "PASSPORT";
	public static final String PERS_PH_EXT = "PERS_PH_EXT";
	public static final String PERS_PH_NBR = "PERS_PH_NBR";
	public static final String PRI_ADDR_1 = "PRI_ADDR_1";
	public static final String PRI_ADDR_2 = "PRI_ADDR_2";
	public static final String PRI_ADDR_3 = "PRI_ADDR_3";
	public static final String PRI_CITY = "PRI_CITY";
	public static final String PRI_POSTAL_CD = "PRI_POSTAL_CD";
	public static final String PRIMARY_OFFICER = "PRIMARY_OFFICER";
	public static final String RACE_CD = "RACE_CD";
	public static final String REG_ADDR_1 = "REG_ADDR_1";
	public static final String REG_ADDR_2 = "REG_ADDR_2";
	public static final String REG_ADDR_3 = "REG_ADDR_3";
	public static final String REG_CITY = "REG_CITY";
	public static final String REG_POSTAL_CD = "REG_POSTAL_CD";
	public static final String RESIDENCE_CD = "RESIDENCE_CD";
	public static final String SALES_VOLUME = "SALES-VOLUME";
	public static final String SALUTATION = "SALUTATION";
	public static final String SHARE_INFO_CD = "SHARE-INFO-CD";
	public static final String SIC_CD_PRI = "SIC_CD_PRI";
	public static final String SIC_CD_SEC = "SIC_CD_SEC";
	public static final String STAT_CD = "STAT_CD";
	public static final String STOCKHLDR_IND = "STOCKHLDR_IND";
	public static final String TAXID = "TAXID";
	public static final String VIP_CD = "VIP_CD";
	public static final String CITZN_ALIEN_CD = "CITZN_ALIEN_CD";
	public static final String NBR_CHILDREN = "NBR_CHILDREN";
	public static final String SPOUSE_NAME = "SPOUSE_NAME";
	public static final String ESTABLISH_DT = "ESTABLISH_DT";
	public static final String NAME_ENG1 = "NAME_ENG1";
	public static final String NAME_ENG2 = "NAME_ENG2";
	public static final String NAME_ENG3 = "NAME_ENG3";
	public static final String ISO_CD = "ISO_CD";
	public static final String SWIFT_CODE = "SWIFT_CODE";
	public static final String FI_CODE = "FI_CODE";
	public static final String OT_CODE = "OT_CODE";
	public static final String INC_CODE = "INC_CODE";
	public static final String SALARY = "SALARY";
	public static final String REFUSED_SERV_IND = "REFUSED_SERV_IND";
	public static final String CITIZN_ID_EXP_DT = "CITIZN_ID_EXP_DT";
	public static final String OPENING_BR = "OPENING_BR";
	public static final String PREFER_BR = "PREFER_BR";
	public static final String MARKETING_STAFF_ID = "MARKETING_STAFF_ID";
	public static final String LOAN_STAFF_ID = "LOAN_STAFF_ID";
	public static final String DEPOSIT_STAFF_ID = "DEPOSIT_STAFF_ID";
	public static final String FIRST_ACCT_OPEN_DT = "FIRST_ACCT_OPEN_DT";
	public static final String DELETED_DT = "DELETED_DT";
	public static final String LAST_ACTIVITY_DT = "LAST_ACTIVITY_DT";
	public static final String INACTIVE_DT = "INACTIVE_DT";
	public static final String LAST_CONTACT_DT = "LAST_CONTACT_DT";
	public static final String LOST_CUST_DT = "LOST_CUST_DT";
	public static final String DEAD_DT = "DEAD_DT";
	public static final String RESIDENT_DT = "RESIDENT_DT";
	public static final String DEPEND_NBR = "DEPEND_NBR";
	public static final String SPOUSE_DOB = "SPOUSE_DOB";
	public static final String SPOUSE_ID = "SPOUSE_ID";
	public static final String CUST_OFFICE_NAME = "CUST_OFFICE_NAME";
	public static final String CUST_JOB_TITLE = "CUST_JOB_TITLE";
	public static final String EMPLOY_STOP_DT = "EMPLOY_STOP_DT";
	public static final String CONTACT_TITLE = "CONTACT_TITLE";
	public static final String CONTACT_RESPONSE = "CONTACT_RESPONSE";
	public static final String BUSINESS_DESC = "BUSINESS_DESC";
	public static final String OWNER_TYPE_CD = "OWNER_TYPE_CD";
	public static final String BUS_PRI_BANK_NAME = "BUS_PRI_BANK_NAME";
	public static final String SMALL_BUSINESS_IND = "SMALL_BUSINESS_IND";
	public static final String CORE_BUS_PCT = "CORE_BUS_PCT";
	public static final String SUB_BUS_PCT = "SUB_BUS_PCT";
	public static final String CREDIT_NEXT_REV_DT = "CREDIT_NEXT_REV_DT";
	public static final String OD_LIMIT = "OD_LIMIT";
	public static final String CC_HOLD_NBR = "CC_HOLD_NBR";
	public static final String DC_HOLD_NBR = "DC_HOLD_NBR";
	public static final String MAX_LN_CREDIT_AMT = "MAX_LN_CREDIT_AMT";
	public static final String COLLECTOR_STAFF_ID = "COLLECTOR_STAFF_ID";
	public static final String BANKRUPT_DT = "BANKRUPT_DT";
	public static final String REMARK_DESC = "REMARK_DESC";
	public static final String REMARK_ADD_EMP_ID = "REMARK_ADD_EMP_ID";
	public static final String REMARK_EXPIRE_DT = "REMARK_EXPIRE_DT";
	public static final String MOBILE_PH_NBR = "MOBILE_PH_NBR";
	public static final String CONTACT_NAME = "CONTACT_NAME";
	public static final String CONTACT_PH_NBR = "CONTACT_PH_NBR";
	public static final String PASSPORT_EXP_DT = "PASSPORT_EXP_DT";
	public static final String DRIV_LIC_EXP_DT = "DRIV_LIC_EXP_DT";
	public static final String TAX_ID_EXP_DT = "TAX_ID_EXP_DT";
	public static final String SC_ID_EXP_DT = "SC_ID_EXP_DT";
	public static final String SWIFT_CODE_EXP_DT = "SWIFT_CODE_EXP_DT";
	public static final String FI_CODE_EXP_DT = "FI_CODE_EXP_DT";
	public static final String OT_CODE_EXP_DT = "OT_CODE_EXP_DT";
	public static final String GOV_CODE = "GOV_CODE";
	public static final String WS_OFF_ADDR_1 = "WS_OFF_ADDR_1";
	public static final String WS_OFF_ADDR_2 = "WS_OFF_ADDR_2";
	public static final String WS_OFF_ADDR_3 = "WS_OFF_ADDR_3";
	public static final String WS_OFF_CITY = "WS_OFF_CITY";
	public static final String WS_OFF_POSTAL_CD = "WS_OFF_POSTAL_CD";
	public static final String RESPONSE_ORG = "RESPONSE_ORG";
	public static final String WORKPERMIT_CARD = "WORKPERMIT_CARD";
	public static final String TEMPORARY_CARD = "TEMPORARY_CARD";
	public static final String WS_AI_CARD = "WS_AI_CARD";
	public static final String WS_AI_EXPDT = "WS_AI_EXPDT";
	public static final String WS_WP_EXPDT = "WS_WP_EXPDT";
	public static final String WS_TC_EXPDT = "WS_TC_EXPDT";
	public static final String OJ_CARD_ID = "OJ_CARD_ID";
	public static final String OJ_EXPT_DT = "OJ_EXPT_DT";
	public static final String OTH_PH_NBR = "OTH_PH_NBR";
	public static final String OTH_PH_EXT = "OTH_PH_EXT";
	public static final String CUSTOMER_SEGMENT = "CUSTOMER_SEGMENT";
	public static final String RO_CODE = "RO_CODE";
	public static final String SUB_SEGMENT = "SUB_SEGMENT";
	public static final String SUB_SME_RETAIL = "SUB_SME_RETAIL";
	public static final String WM_CODE = "WM_CODE";
	public static final String WM_RM = "WM_RM";
	public static final String WM_FX_FLAG = "WM_FX_FLAG";
	public static final String WM_PH_FLAG = "WM_PH_FLAG";
	public static final String CONTACT_CI_1 = "CONTACT_CI_1";
	public static final String CONTACT_MOB_1 = "CONTACT_MOB_1";
	public static final String CONTACT_TITLE_2 = "CONTACT_TITLE_2";
	public static final String CONTACT_NAME_2 = "CONTACT_NAME_2";
	public static final String CONTACT_RESPONSE_2 = "CONTACT_RESPONSE_2";
	public static final String CONTACT_CI_2 = "CONTACT_CI_2";
	public static final String CONTACT_TEL_2 = "CONTACT_TEL_2";
	public static final String CONTACT_MOB_2 = "CONTACT_MOB_2";
	public static final String CONTACT_TITLE_3 = "CONTACT_TITLE_3";
	public static final String CONTACT_NAME_3 = "CONTACT_NAME_3";
	public static final String CONTACT_RESPONSE_3 = "CONTACT_RESPONSE_3";
	public static final String CONTACT_CI_3 = "CONTACT_CI_3";
	public static final String CONTACT_TEL_3 = "CONTACT_TEL_3";
	public static final String CONTACT_MOB_3 = "CONTACT_MOB_3";
	public static final String COPY_ID_CARD_FLAG = "COPY_ID_CARD_FLAG";
	public static final String KYC_FLAG = "KYC_FLAG";
	public static final String IRS_COUNTRY = "IRS_COUNTRY";
	public static final String WS_CI_ISSUE_DT = "WS_CI_ISSUE_DT";
	public static final String WS_PP_ISSUE_DT = "WS_PP_ISSUE_DT";
	public static final String WS_WP_ISSUE_DT = "WS_WP_ISSUE_DT";
	public static final String WS_TN_ISSUE_DT = "WS_TN_ISSUE_DT";
	public static final String WS_AI_ISSUE_DT = "WS_AI_ISSUE_DT";
	public static final String WS_TC_ISSUE_DT = "WS_TC_ISSUE_DT";
	public static final String WS_OT_ISSUE_DT = "WS_OT_ISSUE_DT";
	public static final String WS_SC_ISSUE_DT = "WS_SC_ISSUE_DT";
	public static final String WS_FI_ISSUE_DT = "WS_FI_ISSUE_DT";
	public static final String WS_SW_ISSUE_DT = "WS_SW_ISSUE_DT";
	public static final String WS_OJ_ISSUE_DT = "WS_OJ_ISSUE_DT";
	public static final String WS_PLACE_OF_BIRTH = "WS_PLACE_OF_BIRTH";
	public static final String WS_NATION_2_CD = "WS_NATION_2_CD";
	public static final String WS_INC_CTRY = "WS_INC_CTRY";
	public static final String REG_ISO_CD = "REG_ISO_CD";
	public static final String OFF_ISO_CD = "OFF_ISO_CD";
	public static final String PRI_ADDR_LST_MNT_DATE = "PRI_ADDR_LST_MNT_DATE";
	public static final String REG_ADDR_LST_MNT_DATE = "REG_ADDR_LST_MNT_DATE";
	public static final String OFF_ADDR_LST_MNT_DATE = "OFF_ADDR_LST_MNT_DATE";
	public static final String FATCA_FLAG = "FATCA_FLAG";
	public static final String FATCA_DATE = "FATCA_DATE";
	public static final String FATCA_STATUS = "FATCA_STATUS";
	public static final String US_SOCIAL_NO = "US_SOCIAL_NO";
	public static final String UE_EMPLOYER_ID = "UE_EMPLOYER_ID";
	public static final String UF_FOREIGN_TIN = "UF_FOREIGN_TIN";
	public static final String WS_NAME_MID = "WS_NAME_MID";
	public static final String WS_FAC_ADDR_1 = "WS_FAC_ADDR_1";
	public static final String WS_FAC_ADDR_2 = "WS_FAC_ADDR_2";
	public static final String WS_FAC_ADDR_3 = "WS_FAC_ADDR_3";
	public static final String WS_FAC_CITY = "WS_FAC_CITY";
	public static final String WS_FAC_POSTAL_CD = "WS_FAC_POSTAL_CD";
	public static final String WS_FAC_ISO_CD = "WS_FAC_ISO_CD";
	public static final String NOT_E_CONSOL_FLAG = "NOT_E_CONSOL_FLAG";
	public static final String CC_STMT_FLAG = "CC_STMT_FLAG";
	public static final String RDC_STMT_FLAG = "RDC_STMT_FLAG";
	public static final String C2G_STMT_FLAG = "C2G_STMT_FLAG";
	public static final String W_CONSOL_FLAG = "W_CONSOL_FLAG";
	public static final String LST_UPD_CHANNEL = "LST_UPD_CHANNEL";
	public static final String ENG_ADDR_1 = "ENG_ADDR_1";
	public static final String ENG_ADDR_2 = "ENG_ADDR_2";
	public static final String ENG_ADDR_3 = "ENG_ADDR_3";
	public static final String ENG_CITY = "ENG_CITY";
	public static final String ENG_POSTAL_CD = "ENG_POSTAL_CD";
	public static final String ENG_CTRY_NM = "ENG_CTRY_NM";
	public static final String ENG_CTRY_CD = "ENG_CTRY_CD";
	public static final String GIN_NO = "GIN_NO";

	protected static void buildRMCUSTMapping() throws Exception
	{

		final List<String[]> rmcustFormatlist = CSVUtils.readCSV(Config.getProperties(OutputConstant.INPUT_DIR),
				Config.getProperties(RMCUST_FORMAT_PROP_NAME));

		rmcusteFormatMap = new HashMap<String, String[]>();
		for (String[] rmcustFormat : rmcustFormatlist)
		{
			rmcusteFormatMap.put(rmcustFormat[1].trim(), rmcustFormat);
		}
	}

	private static String extractRMCUSTRecord(final String key, final String line)
	{
		String[] mapField = rmcusteFormatMap.get(key);

		int startPosition = Integer.valueOf(mapField[RMCUST_VALUE_START_POSITION]);
		int endPosition = Integer.valueOf(mapField[RMCUST_VALUE_END_POSITION]);

		// the index will be the value returned at the position -1
		return StringUtils.strip(line.substring(--startPosition, --endPosition));
	}

	private static boolean isMissTitle(final String title, final Collator thaiCollator)
	{
		if (thaiCollator.compare("นางสาว", title.trim()) == 0)
		{
			return true;
		} else
		{
			char[] titleArray = title.trim().toCharArray();
			if ((thaiCollator.compare("น", String.valueOf(titleArray[0])) == 0)
					&& (thaiCollator.compare("ส", String.valueOf(titleArray[2])) == 0))
			{
				return true;
			}
		}
		return false;
	}

	public static String titleCodeConverter(final String title)
	{
		if (title.trim().length() == 0)
		{
			return TITLE_CODE_OTHER;
		} else if (StringUtils.isAsciiPrintable(title))
		{ // for English alphabet
			String eTitle = title.replace(".", "").trim();

			if (eTitle.equalsIgnoreCase(TITLE_MR))
			{
				return TITLE_CODE_MR;
			} else if (eTitle.equalsIgnoreCase(TITLE_MRS))
			{
				return TITLE_CODE_MRS;
			} else if (eTitle.equalsIgnoreCase(TITLE_MS))
			{
				return TITLE_CODE_MS;
			}
		} else
		{ // for Thai alphabet
			final Collator thaiCollator = Collator.getInstance(new Locale("th"));
			if (thaiCollator.compare("นาย", title.trim()) == 0)
			{
				return TITLE_CODE_MR;
			} else if (thaiCollator.compare("นาง", title.trim()) == 0)
			{
				return TITLE_CODE_MRS;
			} else if (isMissTitle(title, thaiCollator))
			{
				return TITLE_CODE_MS;
			}
		}
		return TITLE_CODE_OTHER;
	}

	public static UnitholderUpdate extractRMCUST(final String record)
	{

		final UnitholderUpdate obj = new UnitholderUpdate();
		try
		{
			if (rmcusteFormatMap == null || rmcusteFormatMap.size() == 0)
			{
				buildRMCUSTMapping();
			}

			// TODO, Can't map to output format
			// obj.setiReftype(extractRMCUSTRecord(CUST_TYP_CD, record));
			// TODO TAP
			obj.setiReftype("9"); // use 9 for now
			
			// TODO TAP
			obj.setiRef(extractRMCUSTRecord(CUST_ID, record));

			// 25->20
			obj.setiTax(extractRMCUSTRecord(TAXID, record));
			
			// TODO TAP , Unitholder ID
			obj.setiHolderRef("");

			// Titile from RMCUST and map into code.
			obj.setiTitle(titleCodeConverter(extractRMCUSTRecord(CONTACT_TITLE, record)));
			
			// Not available in TAP
			// TODO if title = 04 use CONTACT_TITLE
			obj.setnTitleT("");

			// TODO TAP
			// obj.setnFirstT(extractRMCUSTRecord(,record));
			// TODO TAP
			// obj.setnLastT(extractRMCUSTRecord(,record));

			// Not available in TAP
			// TODO if possible sent from RMCUST
			// obj.setnTitleE(extractRMCUSTRecord(,record));

			// TODO TAP
			// obj.setnFirstE(extractRMCUSTRecord(NAME_1ST, record));
			// TODO TAP
			// obj.setnLastE(extractRMCUSTRecord(NAME_LST, record));

			// TODO TAP - Person / Corporate
			// obj.setiHldtype(extractRMCUSTRecord(,record));

			// TODO Check length not compatible
			// TODO data also depend on Omnibus flag
			obj.setaAddr1(extractRMCUSTRecord(PRI_ADDR_1, record)
					+ extractRMCUSTRecord(PRI_ADDR_2, record).substring(0, 38).trim());

			// TODO Check length not compatible
			// TODO data also depend on Omnibus flag
			obj.setaAddr2(extractRMCUSTRecord(PRI_ADDR_2, record).substring(39).trim()
					+ extractRMCUSTRecord(PRI_ADDR_3, record));

			// TODO Check length not compatible
			// TODO data also depend on Omnibus flag
			obj.setiZip(extractRMCUSTRecord(PRI_POSTAL_CD, record));

			// TODO Check length not compatible
			obj.setaAddrReg1(extractRMCUSTRecord(REG_ADDR_1, record) + extractRMCUSTRecord(REG_ADDR_2, record).substring(0, 38).trim());

			// TODO Check length not compatible
			obj.setaAddrReg2(extractRMCUSTRecord(REG_ADDR_2, record).substring(39).trim()+extractRMCUSTRecord(REG_ADDR_3, record));

			// TODO Check length not compatible
			obj.setiZipReg(extractRMCUSTRecord(REG_POSTAL_CD, record));

			// Default, THA
			obj.setiCountryReg(THAI_COUNTRY_CODE);

			// TODO missing home ext field
			// TODO add ext if any
			obj.seteHomeTel(extractRMCUSTRecord(PERS_PH_NBR, record));

			// TODO missing ext field
			// TODO add ext if any
			obj.seteOfficeTel(extractRMCUSTRecord(BUS_PHONE_NBR, record));
			obj.seteFax(extractRMCUSTRecord(FAX_PHONE, record));

			// TODO not sure
			obj.seteMobileTel(extractRMCUSTRecord(MOBILE_PH_NBR, record));
			obj.seteEmail(extractRMCUSTRecord(EMAIL_ADDRESS, record));

			obj.setdBirth(Utils.formatDOB(extractRMCUSTRecord(DOB, record)));
			
			// TODO Transform
			String sex = extractRMCUSTRecord(GENDER_CD, record);
			if (!(sex.equalsIgnoreCase(GENER_MALE) || sex.equalsIgnoreCase(GENER_FEMALE)))
			{
				sex = GENER_OTHER;
			}
			obj.setiSex(sex.toUpperCase());

			//  RMCUST use single as 'U', the rest is the same.	
			String maritualStatus = extractRMCUSTRecord(MARIT_CD, record);
			if (maritualStatus.equalsIgnoreCase(RMCUST_MARITAL_STATUS_SINGLE))
			{
				maritualStatus = MARITAL_STATUS_SINGLE;
			}
			obj.setiMaritalStatus(maritualStatus.toUpperCase());

			// TODO WS get from TaxResidenceCountry.CountryCodeValue
			//Coundtry_CD is reusable?
			obj.setiNative("");

			obj.setiOccupation(extractRMCUSTRecord(OCC_CD, record));

			// TODO TAP
			// obj.setdOpen(extractRMCUSTRecord(,record));

			// TODO HARD CODE AS 011 for TMB
			obj.setiAgent(DEFAULT_IAGENT_CODE);

			// TODO Get from TAP
			//obj.setiAgentBranch();

			// TODO to be confirmed, likely to be 'U' for update
			obj.setfStatus(UPDATE_STATUS);

			// TODO TAP
			// obj.setiAdvisor(extractRMCUSTRecord(,record));

			// TODO TAP
			// obj.setfWthdTax(extractRMCUSTRecord(,record));

			// TODO to be confirmed
			boolean flagOmnibus = true; // hardcode for now
			// TODO TAP
			obj.setFlagOmnibus("");

			// TODO to be confirmed / depend on Omninbus flag
			if (!flagOmnibus)
			{
				// obj.setFlagJoin(extractRMCUSTRecord(,record));
			} else
			{
				obj.setFlagJoin(DEFAULT_FLAG_JOINT);
			}

			// TODO TAP
			// obj.setUnitholderName(extractRMCUSTRecord(,record));

			// From Spec hardcode as 'Y'
			obj.setFlagSAGenUHNo(Y);

			// TODO TAP
			// obj.setFundHouse(extractRMCUSTRecord(,record));

			// From Spec Blank
			obj.setuHNoGenByFh(BLANK);

			// TODO TAP
			String fRecieve = "extractRMCUSTRecord(,record)";
			// obj.setfReceive(fRecieve);

			// TODO TAP
			if ("Q".equalsIgnoreCase(fRecieve))
			{
				// obj.setfChqmethod(extractRMCUSTRecord(,record));				
			}

			// TODO TAP + call WS get account name
			obj.seteBankaccRedeem("");
			// TODO TAP
			// obj.setiBankaccRedeem(extractRMCUSTRecord(,record));
			// TODO TAP
			// obj.setiBankRedeem(extractRMCUSTRecord(,record));
			// TODO TAP
			// obj.setiBranchRedeem(extractRMCUSTRecord(,record));
			// TODO TAP
			// obj.setiBankacctypeRedeem(extractRMCUSTRecord(,record));
			// TODO TAP
			// obj.setiBranchNameRedeem(extractRMCUSTRecord(,record));
			// TODO TAP
			// obj.setEstablishRegister(extractRMCUSTRecord(,record));
			// TODO TAP
			// obj.setEstablishNo(extractRMCUSTRecord(,record));

			// TODO Confirm length and business logic
			obj.setaAddrWork1(extractRMCUSTRecord(WS_OFF_ADDR_1, record) + extractRMCUSTRecord(WS_OFF_ADDR_2, record));
			// TODO Confirm length and business logic
			obj.setaAddrWork2(extractRMCUSTRecord(WS_OFF_ADDR_3, record) + extractRMCUSTRecord(WS_OFF_CITY, record));
			obj.setaZipWork(extractRMCUSTRecord(WS_OFF_POSTAL_CD, record));

			// TODO Confirm this date belong to which category
			obj.setIssueDate("");

			// TODO Confirm this date belong to which category
			obj.setExpireDate("");
			obj.setGrossSalary(extractRMCUSTRecord(INC_BAL, record));
			obj.setWorkplaceName(extractRMCUSTRecord(CUST_OFFICE_NAME, record));

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return obj;
	}

}
