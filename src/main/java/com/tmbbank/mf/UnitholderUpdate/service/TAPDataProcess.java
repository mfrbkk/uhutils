package com.tmbbank.mf.UnitholderUpdate.service;

import java.util.List;
import java.util.Set;

import com.tmbbank.mf.UnitholderUpdate.output.domain.UnitholderUpdate;
import com.tmbbank.mf.UnitholderUpdate.utils.Log;

/**
 * 
 * @author Angkoon Pothong
 *
 */
public class TAPDataProcess
{

	public static List<UnitholderUpdate> getUnitholderUpdateDetails(final Set<String> RMID,
			final List<UnitholderUpdate> uhList)
	{

		for (UnitholderUpdate obj : uhList)
		{
			// TODO, Can't map to output format
			obj.setiReftype("");
			obj.setiHolderRef("");
			// TODO can't find mapping for output
			obj.setnTitleT("");
			// TODO TAP
			obj.setnFirstT("");
			// TODO TAP
			obj.setnLastT("");
			// TODO To be confirmed
			obj.setnTitleE("");
			// TODO TAP
			obj.setnFirstE("");
			// TODO TAP
			obj.setnLastE("");
			// TODO TAP
			obj.setiHldtype("");
			// TODO TAP
			obj.setdOpen("");
			// TODO TAP
			obj.setiAgentBranch("");
			// TODO TAP
			obj.setiAdvisor("");

			// TODO TAP
			obj.setfWthdTax("");
			// TODO TAP
			obj.setUnitholderName("");

			// TODO Revise
			obj.setFlagSAGenUHNo("");

			// TODO TAP
			obj.setFundHouse("");
			// TODO TAP
			obj.setfReceive("");

			// TODO To be confirmed
			obj.setfChqmethod("");

			// TODO TAP + call WS get account name
			obj.seteBankaccRedeem("");
			// TODO TAP
			obj.setiBankaccRedeem("");
			// TODO TAP
			obj.setiBankRedeem("");
			// TODO TAP
			obj.setiBranchRedeem("");
			// TODO TAP
			obj.setiBankacctypeRedeem("");
			// TODO TAP
			obj.setiBranchNameRedeem("");
			// TODO TAP
			obj.setEstablishRegister("");
			// TODO TAP
			obj.setEstablishNo("");
			// TODO Confirm this date belong to which category
			obj.setIssueDate("");

			// TODO Confirm this date belong to which category
			obj.setExpireDate("");

			// TODO Not found in RMCUST
			obj.setiCountryReg("");

			// TODO WS get from TaxResidenceCountry.CountryCodeValue
			obj.setiNative("");
		}
		Log.info(uhList.size());
		return uhList;
	}
}
