package com.tmbbank.mf.UnitholderUpdate.database.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tmbbank.mf.UnitholderUpdate.database.domain.RMID;
import com.tmbbank.mf.UnitholderUpdate.utils.Config;
import com.tmbbank.mf.UnitholderUpdate.utils.Log;

/**
 * 
 * @author Angkoon Pothong
 *
 *         ORACLE_SID : MFRTASIT PORT : 1523 MFRTAAPP/MFR01TAAPP ojdbc6.jar
 *
 *  Re
 *         testtap_dev iD,DevName
 */
public class TapDao
{
	public static String DB_USER_PROPERTIY = "DB_USER";
	public static String DB_PASSWORD_PROPERTY = "DB_PASSWORD";
	public static String DB_CONNECTION_PROPERTY = "DB_CONNECTION";

	public static String SQL = "select * from testtap_dev";

	private Connection getConnection()
	{
		try
		{
			// step1 load the driver class
			Class.forName("oracle.jdbc.driver.OracleDriver");

			final String dbConnection = Config.getProperties("DB_CONNECTION_PROPERTY");
			final String dbUser = Config.getProperties("DB_USER_PROPERTIY");
			final String dbPassword = Config.getProperties("DB_PASSWORD_PROPERTY");

			final Connection conn = DriverManager.getConnection(dbConnection, dbUser, dbPassword);

			if (conn != null)
			{
				Log.debug("SUCCESSFULLY CONNECT TO [" + dbConnection + "]");
			} else
			{
				Log.err("Failed to make connection! to [" + dbConnection + "]");
			}

			return conn;

		} catch (final Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
	}

	public List<RMID> getUpdatedRMIDList()
	{

		List<RMID> rmidList = null;
		final Connection conn = getConnection();
		try
		{
			final Statement stmt = conn.createStatement();
			final ResultSet rs = stmt.executeQuery(SQL);

			if (rs != null)
			{
				rmidList = new ArrayList<RMID>();
				Log.debug("SUCCESSFULLY CALL [" + SQL + "]\n\n");
				Log.debug("iD | DevName");
				while (rs.next())
				{
					final RMID record = new RMID();
					record.setId(rs.getInt(1));

					rmidList.add(record);

					Log.debug(rs.getInt(1) + "   " + rs.getString(2));
				}
			}

			conn.close();

		} catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return rmidList;
	}

}
