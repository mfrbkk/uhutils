package com.tmbbank.mf.UnitholderUpdate.main;

import java.util.List;
import java.util.Set;

import com.tmbbank.mf.UnitholderUpdate.output.domain.UnitholderUpdate;
import com.tmbbank.mf.UnitholderUpdate.output.txt.TxtProducer;
import com.tmbbank.mf.UnitholderUpdate.service.CBSDataProcess;
import com.tmbbank.mf.UnitholderUpdate.service.RMCUSTExtractor;
import com.tmbbank.mf.UnitholderUpdate.service.TAPDataProcess;
import com.tmbbank.mf.UnitholderUpdate.utils.Log;

/**
 * 
 * @author Angkoon Pothong
 *
 */
public class Exec
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub

		Log.debug("============= S T A R T   P R O C E S S ============");

		System.out.println( RMCUSTExtractor.titleCodeConverter("MR"));
		System.out.println( RMCUSTExtractor.titleCodeConverter("MR."));
		System.out.println( RMCUSTExtractor.titleCodeConverter("MRS "));
		System.out.println( RMCUSTExtractor.titleCodeConverter("Ms."));
		System.out.println( RMCUSTExtractor.titleCodeConverter("   "));
		System.out.println( RMCUSTExtractor.titleCodeConverter("555"));
		System.out.println( RMCUSTExtractor.titleCodeConverter(""));

		// Collator thai = Collator.getInstance(new Locale("th"));
		// System.out.println( thai.compare("��", "�����") );
		// System.out.println( thai.compare("�����", "�����") );
		// System.out.println( "��".compareTo("�����") );
		// CSVUtils.readCSV();
		
		final String dir = System.getProperty("user.dir");
        System.out.println("current dir = " + dir);
		
		Log.blDebugMode = Boolean.FALSE;

		Set<String> MFRMIDList = CBSDataProcess.getMFRMID();

		List<UnitholderUpdate> uhList = CBSDataProcess.getMFRecordList(MFRMIDList);

		TAPDataProcess.getUnitholderUpdateDetails(MFRMIDList, uhList);

		TxtProducer.createFile(TxtProducer.formatData(uhList));

	}

}
