package com.tmbbank.mf.UnitholderUpdate.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 
 * @author Angkoon Pothong
 *
 */
public class Config
{

	public static final String configFileName = "UHUpdate.properties";

	private static Properties prop;

	public static String getProperties(final String propName) throws Exception
	{

		InputStream input = null;
		try
		{
			if (prop == null)
			{
				prop = new Properties();

				if (FileUtils.fileExist(configFileName))
				{
					input = new FileInputStream(configFileName);
					prop.load(input);

				} else
				{
					throw new Exception("Config file [" + configFileName + "] is not found");
				}
			}

			return prop.getProperty(propName);

		} catch (IOException ex)
		{
			ex.printStackTrace();
			throw ex;
		} finally
		{
			if (input != null)
			{
				try
				{
					input.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

	}

}
