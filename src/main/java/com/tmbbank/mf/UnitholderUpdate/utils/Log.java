package com.tmbbank.mf.UnitholderUpdate.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Log
{

	private static final String MSG_DATETIME_FORMAT = "EEE yyyy/MM/dd HH:mm:ss:SSS";
	private static final SimpleDateFormat sdf = new SimpleDateFormat(MSG_DATETIME_FORMAT, Locale.US);

	public static Boolean blDebugMode = Boolean.FALSE;

	public static void info(final String msg)
	{
		System.out.println(sdf.format(new Date()) + "  - [INFO] - " + msg);
	}

	public static void info(final Integer msg)
	{
		System.out.println(sdf.format(new Date()) + "  - [INFO] - " + String.valueOf(msg));
	}

	public static void debug(final String msg)
	{
		if (blDebugMode)
		{
			System.out.println(sdf.format(new Date()) + "  - [DEBUG] - " + msg);
		}
	}

	public static void err(final String msg)
	{

		System.err.println(sdf.format(new Date()) + "  - [ERROR] - " + msg);
	}
}
