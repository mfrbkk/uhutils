package com.tmbbank.mf.UnitholderUpdate.utils;

/**
 * 
 * @author Angkoon Pothong
 *
 */
public class Utils
{
	public static String paddingLeft(final String input, final int fieldLength, final Character paddingChar,
			final boolean blLeftPadding) throws Exception
	{
		String temp = input.trim();
		String result = "";

		if (temp.length() == fieldLength)
		{
			result = temp;
		} else if (temp.length() > fieldLength)
		{
			throw new Exception(
					"input's length [" + temp.length() + "] is larger than fieldLength [" + fieldLength + "]");
		} else
		{

		}

		return result;
	}

	public static void setLineBreakChar(final String OS)
	{
		if (("UNIX").equalsIgnoreCase(OS))
		{
			System.setProperty("line.separator", "\n");
		} else if (("WINDOWS").equalsIgnoreCase(OS))
		{
			System.setProperty("line.separator", "\r\n");
		} else
		{
			// Do nothing;
		}

	}

	public static String formatDOB(String b_date)
	{
		if (b_date.length() != 8)
		{
			Log.err("Cannot Transform DOB ["+b_date+"] expect 8 digits" );
			return "";
		}
		return b_date.substring(0, 3) + "-" + b_date.substring(4, 5) + "-" + b_date.substring(6, 7);
	}
}
