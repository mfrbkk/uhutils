package com.tmbbank.mf.UnitholderUpdate.utils;

import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtils
{
	public static boolean fileExist(final String filename)
	{
		return Files.exists(Paths.get(filename));
	}
}
