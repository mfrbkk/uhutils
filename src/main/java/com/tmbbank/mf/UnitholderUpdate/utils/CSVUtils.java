package com.tmbbank.mf.UnitholderUpdate.utils;

import java.io.FileReader;
import java.util.List;

import com.opencsv.CSVReader;

/**
 * 
 * @author Angkoon Pothong
 *
 */
public class CSVUtils
{
	public static List<String[]> readCSV(final String inputPath, final String fileName)
	{
		CSVReader reader;
		List<String[]> myEntries = null;
		try
		{
			reader = new CSVReader(new FileReader(inputPath.trim().concat(fileName)));
			myEntries = reader.readAll();

			for (String[] entry : myEntries)
			{
				Log.debug("[" + entry[0] + "] [" + entry[1] + "] [" + entry[2] + "] [" + entry[3] + "] [" + entry[4]
						+ "] [" + entry[5] + "]");
			}

		} catch (final Exception e)
		{
			e.printStackTrace();
		}

		return myEntries;
	}
}
