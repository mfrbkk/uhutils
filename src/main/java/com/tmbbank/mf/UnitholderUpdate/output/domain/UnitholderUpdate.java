package com.tmbbank.mf.UnitholderUpdate.output.domain;

/**
 * 
 * @author Angkoon Pothong
 *
 */
public class UnitholderUpdate
{

	private String iReftype;
	private String iRef;
	private String iTax;
	private String iHolderRef;
	private String iTitle;
	private String nTitleT;
	private String nFirstT;
	private String nLastT;
	private String nTitleE;
	private String nFirstE;
	private String nLastE;
	private String iHldtype;
	private String aAddr1;
	private String aAddr2;
	private String iZip;
	private String aAddrReg1;
	private String aAddrReg2;
	private String iZipReg;
	private String iCountryReg;
	private String eHomeTel;
	private String eOfficeTel;
	private String eFax;
	private String eMobileTel;
	private String eEmail;
	private String dBirth;
	private String iSex;
	private String iMaritalStatus;
	private String iNative;
	private String iOccupation;
	private String dOpen;
	private String iAgent;
	private String iAgentBranch;
	private String fStatus;
	private String iAdvisor;
	private String fWthdTax;
	private String flagOmnibus;
	private String flagJoin;
	private String unitholderName;
	private String flagSAGenUHNo;
	private String fundHouse;
	private String uHNoGenByFh;
	private String fReceive;
	private String fChqmethod;
	private String eBankaccRedeem;
	private String iBankaccRedeem;
	private String iBankRedeem;
	private String iBranchRedeem;
	private String iBankacctypeRedeem;
	private String iBranchNameRedeem;
	private String establishRegister;
	private String establishNo;
	private String aAddrWork1;
	private String aAddrWork2;
	private String aZipWork;
	private String issueDate;
	private String expireDate;
	private String grossSalary;
	private String workplaceName;

	public final String getiReftype()
	{
		return iReftype;
	}

	public final void setiReftype(String iReftype)
	{
		this.iReftype = iReftype;
	}

	public final String getiRef()
	{
		return iRef;
	}

	public final void setiRef(String iRef)
	{
		this.iRef = iRef;
	}

	public final String getiTax()
	{
		return iTax;
	}

	public final void setiTax(String iTax)
	{
		this.iTax = iTax;
	}

	public final String getiHolderRef()
	{
		return iHolderRef;
	}

	public final void setiHolderRef(String iHolderRef)
	{
		this.iHolderRef = iHolderRef;
	}

	public final String getiTitle()
	{
		return iTitle;
	}

	public final void setiTitle(String iTitle)
	{
		this.iTitle = iTitle;
	}

	public final String getnTitleT()
	{
		return nTitleT;
	}

	public final void setnTitleT(String nTitleT)
	{
		this.nTitleT = nTitleT;
	}

	public final String getnFirstT()
	{
		return nFirstT;
	}

	public final void setnFirstT(String nFirstT)
	{
		this.nFirstT = nFirstT;
	}

	public final String getnLastT()
	{
		return nLastT;
	}

	public final void setnLastT(String nLastT)
	{
		this.nLastT = nLastT;
	}

	public final String getnTitleE()
	{
		return nTitleE;
	}

	public final void setnTitleE(String nTitleE)
	{
		this.nTitleE = nTitleE;
	}

	public final String getnFirstE()
	{
		return nFirstE;
	}

	public final void setnFirstE(String nFirstE)
	{
		this.nFirstE = nFirstE;
	}

	public final String getnLastE()
	{
		return nLastE;
	}

	public final void setnLastE(String nLastE)
	{
		this.nLastE = nLastE;
	}

	public final String getiHldtype()
	{
		return iHldtype;
	}

	public final void setiHldtype(String iHldtype)
	{
		this.iHldtype = iHldtype;
	}

	public final String getaAddr1()
	{
		return aAddr1;
	}

	public final void setaAddr1(String aAddr1)
	{
		this.aAddr1 = aAddr1;
	}

	public final String getaAddr2()
	{
		return aAddr2;
	}

	public final void setaAddr2(String aAddr2)
	{
		this.aAddr2 = aAddr2;
	}

	public final String getiZip()
	{
		return iZip;
	}

	public final void setiZip(String iZip)
	{
		this.iZip = iZip;
	}

	public final String getaAddrReg1()
	{
		return aAddrReg1;
	}

	public final void setaAddrReg1(String aAddrReg1)
	{
		this.aAddrReg1 = aAddrReg1;
	}

	public final String getaAddrReg2()
	{
		return aAddrReg2;
	}

	public final void setaAddrReg2(String aAddrReg2)
	{
		this.aAddrReg2 = aAddrReg2;
	}

	public final String getiZipReg()
	{
		return iZipReg;
	}

	public final void setiZipReg(String iZipReg)
	{
		this.iZipReg = iZipReg;
	}

	public final String getiCountryReg()
	{
		return iCountryReg;
	}

	public final void setiCountryReg(String iCountryReg)
	{
		this.iCountryReg = iCountryReg;
	}

	public final String geteHomeTel()
	{
		return eHomeTel;
	}

	public final void seteHomeTel(String eHomeTel)
	{
		this.eHomeTel = eHomeTel;
	}

	public final String geteOfficeTel()
	{
		return eOfficeTel;
	}

	public final void seteOfficeTel(String eOfficeTel)
	{
		this.eOfficeTel = eOfficeTel;
	}

	public final String geteFax()
	{
		return eFax;
	}

	public final void seteFax(String eFax)
	{
		this.eFax = eFax;
	}

	public final String geteMobileTel()
	{
		return eMobileTel;
	}

	public final void seteMobileTel(String eMobileTel)
	{
		this.eMobileTel = eMobileTel;
	}

	public final String geteEmail()
	{
		return eEmail;
	}

	public final void seteEmail(String eEmail)
	{
		this.eEmail = eEmail;
	}

	public final String getdBirth()
	{
		return dBirth;
	}

	public final void setdBirth(String dBirth)
	{
		this.dBirth = dBirth;
	}

	public final String getiSex()
	{
		return iSex;
	}

	public final void setiSex(String iSex)
	{
		this.iSex = iSex;
	}

	public final String getiMaritalStatus()
	{
		return iMaritalStatus;
	}

	public final void setiMaritalStatus(String iMaritalStatus)
	{
		this.iMaritalStatus = iMaritalStatus;
	}

	public final String getiNative()
	{
		return iNative;
	}

	public final void setiNative(String iNative)
	{
		this.iNative = iNative;
	}

	public final String getiOccupation()
	{
		return iOccupation;
	}

	public final void setiOccupation(String iOccupation)
	{
		this.iOccupation = iOccupation;
	}

	public final String getdOpen()
	{
		return dOpen;
	}

	public final void setdOpen(String dOpen)
	{
		this.dOpen = dOpen;
	}

	public final String getiAgent()
	{
		return iAgent;
	}

	public final void setiAgent(String iAgent)
	{
		this.iAgent = iAgent;
	}

	public final String getiAgentBranch()
	{
		return iAgentBranch;
	}

	public final void setiAgentBranch(String iAgentBranch)
	{
		this.iAgentBranch = iAgentBranch;
	}

	public final String getfStatus()
	{
		return fStatus;
	}

	public final void setfStatus(String fStatus)
	{
		this.fStatus = fStatus;
	}

	public final String getiAdvisor()
	{
		return iAdvisor;
	}

	public final void setiAdvisor(String iAdvisor)
	{
		this.iAdvisor = iAdvisor;
	}

	public final String getfWthdTax()
	{
		return fWthdTax;
	}

	public final void setfWthdTax(String fWthdTax)
	{
		this.fWthdTax = fWthdTax;
	}

	public final String getFlagOmnibus()
	{
		return flagOmnibus;
	}

	public final void setFlagOmnibus(String flagOmnibus)
	{
		this.flagOmnibus = flagOmnibus;
	}

	public final String getFlagJoin()
	{
		return flagJoin;
	}

	public final void setFlagJoin(String flagJoin)
	{
		this.flagJoin = flagJoin;
	}

	public final String getUnitholderName()
	{
		return unitholderName;
	}

	public final void setUnitholderName(String unitholderName)
	{
		this.unitholderName = unitholderName;
	}

	public final String getFlagSAGenUHNo()
	{
		return flagSAGenUHNo;
	}

	public final void setFlagSAGenUHNo(String flagSAGenUHNo)
	{
		this.flagSAGenUHNo = flagSAGenUHNo;
	}

	public final String getFundHouse()
	{
		return fundHouse;
	}

	public final void setFundHouse(String fundHouse)
	{
		this.fundHouse = fundHouse;
	}

	public final String getuHNoGenByFh()
	{
		return uHNoGenByFh;
	}

	public final void setuHNoGenByFh(String uHNoGenByFh)
	{
		this.uHNoGenByFh = uHNoGenByFh;
	}

	public final String getfReceive()
	{
		return fReceive;
	}

	public final void setfReceive(String fReceive)
	{
		this.fReceive = fReceive;
	}

	public final String getfChqmethod()
	{
		return fChqmethod;
	}

	public final void setfChqmethod(String fChqmethod)
	{
		this.fChqmethod = fChqmethod;
	}

	public final String geteBankaccRedeem()
	{
		return eBankaccRedeem;
	}

	public final void seteBankaccRedeem(String eBankaccRedeem)
	{
		this.eBankaccRedeem = eBankaccRedeem;
	}

	public final String getiBankaccRedeem()
	{
		return iBankaccRedeem;
	}

	public final void setiBankaccRedeem(String iBankaccRedeem)
	{
		this.iBankaccRedeem = iBankaccRedeem;
	}

	public final String getiBankRedeem()
	{
		return iBankRedeem;
	}

	public final void setiBankRedeem(String iBankRedeem)
	{
		this.iBankRedeem = iBankRedeem;
	}

	public final String getiBranchRedeem()
	{
		return iBranchRedeem;
	}

	public final void setiBranchRedeem(String iBranchRedeem)
	{
		this.iBranchRedeem = iBranchRedeem;
	}

	public final String getiBankacctypeRedeem()
	{
		return iBankacctypeRedeem;
	}

	public final void setiBankacctypeRedeem(String iBankacctypeRedeem)
	{
		this.iBankacctypeRedeem = iBankacctypeRedeem;
	}

	public final String getiBranchNameRedeem()
	{
		return iBranchNameRedeem;
	}

	public final void setiBranchNameRedeem(String iBranchNameRedeem)
	{
		this.iBranchNameRedeem = iBranchNameRedeem;
	}

	public final String getEstablishRegister()
	{
		return establishRegister;
	}

	public final void setEstablishRegister(String establishRegister)
	{
		this.establishRegister = establishRegister;
	}

	public final String getEstablishNo()
	{
		return establishNo;
	}

	public final void setEstablishNo(String establishNo)
	{
		this.establishNo = establishNo;
	}

	public final String getaAddrWork1()
	{
		return aAddrWork1;
	}

	public final void setaAddrWork1(String aAddrWork1)
	{
		this.aAddrWork1 = aAddrWork1;
	}

	public final String getaAddrWork2()
	{
		return aAddrWork2;
	}

	public final void setaAddrWork2(String aAddrWork2)
	{
		this.aAddrWork2 = aAddrWork2;
	}

	public final String getaZipWork()
	{
		return aZipWork;
	}

	public final void setaZipWork(String aZipWork)
	{
		this.aZipWork = aZipWork;
	}

	public final String getIssueDate()
	{
		return issueDate;
	}

	public final void setIssueDate(String issueDate)
	{
		this.issueDate = issueDate;
	}

	public final String getExpireDate()
	{
		return expireDate;
	}

	public final void setExpireDate(String expireDate)
	{
		this.expireDate = expireDate;
	}

	public final String getGrossSalary()
	{
		return grossSalary;
	}

	public final void setGrossSalary(String grossSalary)
	{
		this.grossSalary = grossSalary;
	}

	public final String getWorkplaceName()
	{
		return workplaceName;
	}

	public final void setWorkplaceName(String workplaceName)
	{
		this.workplaceName = workplaceName;
	}

	public final String toString()
	{
		final StringBuilder sb = new StringBuilder();

		sb.append("iReftype [" + iReftype + "], ");
		sb.append("iRef [" + iRef + "], ");
		sb.append("iTax [" + iTax + "], ");
		sb.append("iHolderRef [" + iHolderRef + "], ");
		sb.append("iTitle [" + iTitle + "], ");
		sb.append("nTitleT [" + nTitleT + "], ");
		sb.append("nFirstT [" + nFirstT + "], ");
		sb.append("nLastT [" + nLastT + "], ");
		sb.append("nTitleE [" + nTitleE + "], ");
		sb.append("nFirstE [" + nFirstE + "], ");
		sb.append("nLastE [" + nLastE + "], ");
		sb.append("iHldtype [" + iHldtype + "], ");
		sb.append("aAddr1 [" + aAddr1 + "], ");
		sb.append("aAddr2 [" + aAddr2 + "], ");
		sb.append("iZip [" + iZip + "], ");
		sb.append("aAddrReg1 [" + aAddrReg1 + "], ");
		sb.append("aAddrReg2 [" + aAddrReg2 + "], ");
		sb.append("iZipReg [" + iZipReg + "], ");
		sb.append("iCountryReg [" + iCountryReg + "], ");
		sb.append("eHomeTel [" + eHomeTel + "], ");
		sb.append("eOfficeTel [" + eOfficeTel + "], ");
		sb.append("eFax [" + eFax + "], ");
		sb.append("eMobileTel [" + eMobileTel + "], ");
		sb.append("eEmail [" + eEmail + "], ");
		sb.append("dBirth [" + dBirth + "], ");
		sb.append("iSex [" + iSex + "], ");
		sb.append("iMaritalStatus [" + iMaritalStatus + "], ");
		sb.append("iNative [" + iNative + "], ");
		sb.append("iOccupation [" + iOccupation + "], ");
		sb.append("dOpen [" + dOpen + "], ");
		sb.append("iAgent [" + iAgent + "], ");
		sb.append("iAgentBranch [" + iAgentBranch + "], ");
		sb.append("fStatus [" + fStatus + "], ");
		sb.append("iAdvisor [" + iAdvisor + "], ");
		sb.append("fWthdTax [" + fWthdTax + "], ");
		sb.append("flagOmnibus [" + flagOmnibus + "], ");
		sb.append("flagJoin [" + flagJoin + "], ");
		sb.append("unitholderName [" + unitholderName + "], ");
		sb.append("flagSAGenUHNo [" + flagSAGenUHNo + "], ");
		sb.append("fundHouse [" + fundHouse + "], ");
		sb.append("uHNoGenByFh [" + uHNoGenByFh + "], ");
		sb.append("fReceive [" + fReceive + "], ");
		sb.append("fChqmethod [" + fChqmethod + "], ");
		sb.append("eBankaccRedeem [" + eBankaccRedeem + "], ");
		sb.append("iBankaccRedeem [" + iBankaccRedeem + "], ");
		sb.append("iBankRedeem [" + iBankRedeem + "] ");
		sb.append("iBranchRedeem [" + iBranchRedeem + "], ");
		sb.append("iBankacctypeRedeem [" + iBankacctypeRedeem + "], ");
		sb.append("iBranchNameRedeem [" + iBranchNameRedeem + "], ");
		sb.append("establishRegister [" + establishRegister + "], ");
		sb.append("establishNo [" + establishNo + "], ");
		sb.append("aAddrWork1 [" + aAddrWork1 + "], ");
		sb.append("aAddrWork2 [" + aAddrWork2 + "], ");
		sb.append("aZipWork [" + aZipWork + "], ");
		sb.append("issueDate [" + issueDate + "], ");
		sb.append("expireDate [" + expireDate + "], ");
		sb.append("grossSalary [" + grossSalary + "], ");
		sb.append("workplaceName [" + workplaceName + "]");

		return sb.toString();
	}
}
