package com.tmbbank.mf.UnitholderUpdate.output.txt;
/**
 * 
 * @author Angkoon Pothong
 *
 */

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import com.tmbbank.mf.UnitholderUpdate.output.constant.OutputConstant;
import com.tmbbank.mf.UnitholderUpdate.output.constant.UnitholderUpdateLenght;
import com.tmbbank.mf.UnitholderUpdate.output.domain.UnitholderUpdate;
import com.tmbbank.mf.UnitholderUpdate.utils.Config;
import com.tmbbank.mf.UnitholderUpdate.utils.Log;
import com.tmbbank.mf.UnitholderUpdate.utils.Utils;

public class TxtProducer
{

//	public static final Charset CHARSET = Charset.forName("UTF-8");
	public static final Charset CHARSET = Charset.forName("TIS-620");

	private static String getFilename()
	{
		final SimpleDateFormat sdf = new SimpleDateFormat(OutputConstant.OUTPUT_DATETIME_FORMAT, Locale.US);

		String filename = OutputConstant.OUTPUT_PATTERN;
		return filename.replace(OutputConstant.OUTPUT_DATETIME_PATTERN, sdf.format(new Date()));
	}

	/**
	 * This method will create a new folder if the expected folder doesn't exist
	 * 
	 * @return output dir from preperties
	 * @throws Exception
	 */
	public static String getoutputPath() throws Exception
	{

		final String outputDir = Config.getProperties(OutputConstant.OUTPUT_DIR);

		final Path dir = Paths.get(outputDir);

		if (Files.notExists(dir))
		{
			try
			{
				Files.createDirectories(dir);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
			Log.debug(dir.toString() + "is created");
		}

		return outputDir + getFilename();
	}

	public static void createFile(final List<String> content)
	{
		try
		{
			Path file = Paths.get(getoutputPath());

			if (Files.exists(file))
			{
				Log.debug("[" + file.toFile() + "] is already existed");
				Log.debug("Wait 1 minute to generate new file");
				Thread.sleep(60000);
				file = Paths.get(getoutputPath());
				Log.debug("New file name generated....");
			}

			Utils.setLineBreakChar(Config.getProperties(OutputConstant.LINE_BREAK_OS));

			Path out = Files.write(file, content, CHARSET);

			Log.debug("[" + out.toString() + "] IS BEING PRODUCED");
			Log.debug("============= E N D   P R O C E S S ============");

		} catch (Exception e)
		{

			e.printStackTrace();
		}

	}

	public static List<String> formatData(final List<UnitholderUpdate> unitholderUpdateList)
	{
		List<String> output = new ArrayList<String>();

		int count = 0;
		for (final UnitholderUpdate record : unitholderUpdateList)
		{
			StringBuilder sb = new StringBuilder();

			sb.append(StringUtils.rightPad(record.getiReftype(), UnitholderUpdateLenght.I_REF_TYPE));
			sb.append(StringUtils.rightPad(record.getiRef(), UnitholderUpdateLenght.I_REF));
			sb.append(StringUtils.rightPad(record.getiTax(), UnitholderUpdateLenght.I_TAX));
			sb.append(StringUtils.rightPad(record.getiHolderRef(), UnitholderUpdateLenght.I_HOLDER_REF));
			sb.append(StringUtils.rightPad(record.getiTitle(), UnitholderUpdateLenght.I_TITLE));
			sb.append(StringUtils.rightPad(record.getnTitleT(), UnitholderUpdateLenght.N_TITLE_T));
			sb.append(StringUtils.rightPad(record.getnFirstT(), UnitholderUpdateLenght.N_FIRST_T));
			sb.append(StringUtils.rightPad(record.getnLastT(), UnitholderUpdateLenght.N_LAST_T));
			sb.append(StringUtils.rightPad(record.getnTitleE(), UnitholderUpdateLenght.N_TITLE_E));
			sb.append(StringUtils.rightPad(record.getnFirstE(), UnitholderUpdateLenght.N_FIRST_E));
			sb.append(StringUtils.rightPad(record.getnLastE(), UnitholderUpdateLenght.N_LAST_E));
			sb.append(StringUtils.rightPad(record.getiHldtype(), UnitholderUpdateLenght.I_HLD_TYPE));
			sb.append(StringUtils.rightPad(record.getaAddr1(), UnitholderUpdateLenght.A_ADDR_1));
			sb.append(StringUtils.rightPad(record.getaAddr2(), UnitholderUpdateLenght.A_ADDR_2));
			sb.append(StringUtils.rightPad(record.getiZip(), UnitholderUpdateLenght.I_ZIP));
			sb.append(StringUtils.rightPad(record.getaAddrReg1(), UnitholderUpdateLenght.A_ADDR_REG_1));
			sb.append(StringUtils.rightPad(record.getaAddrReg2(), UnitholderUpdateLenght.A_ADDR_REG_2));
			sb.append(StringUtils.rightPad(record.getiZipReg(), UnitholderUpdateLenght.I_ZIP_REG));
			sb.append(StringUtils.rightPad(record.getiCountryReg(), UnitholderUpdateLenght.I_COUNTRY_REG));
			sb.append(StringUtils.rightPad(record.geteHomeTel(), UnitholderUpdateLenght.E_HOME_TEL));
			sb.append(StringUtils.rightPad(record.geteOfficeTel(), UnitholderUpdateLenght.E_OFFICE_TEL));
			sb.append(StringUtils.rightPad(record.geteFax(), UnitholderUpdateLenght.E_FAX));
			sb.append(StringUtils.rightPad(record.geteMobileTel(), UnitholderUpdateLenght.E_MOBILE_TEL));
			sb.append(StringUtils.rightPad(record.geteEmail(), UnitholderUpdateLenght.E_EMAIL));
			sb.append(StringUtils.rightPad(record.getdBirth(), UnitholderUpdateLenght.D_BIRTH));
			sb.append(StringUtils.rightPad(record.getiSex(), UnitholderUpdateLenght.I_SEX));
			sb.append(StringUtils.rightPad(record.getiMaritalStatus(), UnitholderUpdateLenght.I_MARITAL_STATUS));
			sb.append(StringUtils.rightPad(record.getiNative(), UnitholderUpdateLenght.I_NATIVE));
			sb.append(StringUtils.rightPad(record.getiOccupation(), UnitholderUpdateLenght.I_OCCUPATION));
			sb.append(StringUtils.rightPad(record.getdOpen(), UnitholderUpdateLenght.D_OPEN));
			sb.append(StringUtils.rightPad(record.getiAgent(), UnitholderUpdateLenght.I_AGENT));
			sb.append(StringUtils.rightPad(record.getiAgentBranch(), UnitholderUpdateLenght.I_AGENTBRANCH));
			sb.append(StringUtils.rightPad(record.getfStatus(), UnitholderUpdateLenght.F_STATUS));
			sb.append(StringUtils.rightPad(record.getiAdvisor(), UnitholderUpdateLenght.I_ADVISOR));
			sb.append(StringUtils.rightPad(record.getfWthdTax(), UnitholderUpdateLenght.F_WTHDTAX));
			sb.append(StringUtils.rightPad(record.getFlagOmnibus(), UnitholderUpdateLenght.FLAG_OMNIBUS));
			sb.append(StringUtils.rightPad(record.getFlagJoin(), UnitholderUpdateLenght.FLAG_JOIN));
			sb.append(StringUtils.rightPad(record.getUnitholderName(), UnitholderUpdateLenght.UNITHOLDER_NAME));
			sb.append(StringUtils.rightPad(record.getFlagSAGenUHNo(), UnitholderUpdateLenght.FLAG_SA_GEN_UH_NO));
			sb.append(StringUtils.rightPad(record.getFundHouse(), UnitholderUpdateLenght.FUND_HOUSE));
			sb.append(StringUtils.rightPad(record.getuHNoGenByFh(), UnitholderUpdateLenght.UH_NO_GEN_BY_FH));
			sb.append(StringUtils.rightPad(record.getfReceive(), UnitholderUpdateLenght.F_RECEIVE));
			sb.append(StringUtils.rightPad(record.getfChqmethod(), UnitholderUpdateLenght.F_CHQ_METHOD));
			sb.append(StringUtils.rightPad(record.geteBankaccRedeem(), UnitholderUpdateLenght.E_BANK_ACC_REDEEM));
			sb.append(StringUtils.rightPad(record.getiBankaccRedeem(), UnitholderUpdateLenght.I_BANK_ACC_REDEEM));
			sb.append(StringUtils.rightPad(record.getiBankRedeem(), UnitholderUpdateLenght.I_BANK_REDEEM));
			sb.append(StringUtils.rightPad(record.getiBranchRedeem(), UnitholderUpdateLenght.I_BRANCH_CH_REDEEM));
			sb.append(StringUtils.rightPad(record.getiBankacctypeRedeem(),
					UnitholderUpdateLenght.I_BANK_ACC_TYPE_REDEEM));
			sb.append(StringUtils.rightPad(record.getiBranchNameRedeem(), UnitholderUpdateLenght.I_BRANCH_NAME_REDEEM));
			sb.append(StringUtils.rightPad(record.getEstablishRegister(), UnitholderUpdateLenght.ESTABLISH_REGISTER));
			sb.append(StringUtils.rightPad(record.getEstablishNo(), UnitholderUpdateLenght.ESTABLISH_NO));
			sb.append(StringUtils.rightPad(record.getaAddrWork1(), UnitholderUpdateLenght.A_ADDR_WORK_1));
			sb.append(StringUtils.rightPad(record.getaAddrWork2(), UnitholderUpdateLenght.A_ADDR_WORK_2));
			sb.append(StringUtils.rightPad(record.getaZipWork(), UnitholderUpdateLenght.A_ZIP_WORK));
			sb.append(StringUtils.rightPad(record.getIssueDate(), UnitholderUpdateLenght.ISSUE_DATE));
			sb.append(StringUtils.rightPad(record.getExpireDate(), UnitholderUpdateLenght.EXPIRE_DATE));
			sb.append(StringUtils.rightPad(record.getGrossSalary(), UnitholderUpdateLenght.GROSS_SALARY));
			sb.append(StringUtils.rightPad(record.getWorkplaceName(), UnitholderUpdateLenght.WORKPLACE_NAME));

			output.add(sb.toString());
			Log.info(record.toString());
			if (++count == 2)
			{
				break;
			}
		}

		return output;
	}

}
