package com.tmbbank.mf.UnitholderUpdate.output.constant;

/**
 * 
 * @author Angkoon Pothong
 *
 */
public class OutputConstant
{

	public static final String OUTPUT_PATTERN = "unitholderccyymmddhhmm.txt";
	public static final String OUTPUT_DATETIME_PATTERN = "ccyymmddhhmm";
	public static final String OUTPUT_DATETIME_FORMAT = "yyyyMMddHHmm";
	public static final String OUTPUT_DIR = "outputDir";
	public static final String INPUT_DIR = "inputDir";

	public static final String LINE_BREAK_OS = "lineBreakOS";

}
